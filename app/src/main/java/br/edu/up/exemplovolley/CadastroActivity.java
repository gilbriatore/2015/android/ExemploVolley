package br.edu.up.exemplovolley;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CadastroActivity extends AppCompatActivity {

  private EditText txtId;
  private EditText txtNome;
  private EditText txtRg;
  private EditText txtCpf;
  private EditText txtEstado;
  private EditText txtSexo;
  private EditText txtData;
  private EditText txtLocal;

  //private final String SERVIDOR = "http://10.0.2.2:9090";
  private final String SERVIDOR = "http://ws-briatore.rhcloud.com";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cadastro);

    txtId = (EditText) findViewById(R.id.txtId);
    txtNome = (EditText) findViewById(R.id.txtNome);
    txtRg = (EditText) findViewById(R.id.txtRg);
    txtCpf = (EditText) findViewById(R.id.txtCpf);
    txtEstado = (EditText) findViewById(R.id.txtEstadoCivil);
    txtSexo = (EditText) findViewById(R.id.txtSexo);
    txtData = (EditText) findViewById(R.id.txtData);
    txtLocal = (EditText) findViewById(R.id.txtLocal);
  }

  public void onClickLimpar(View v) {
    limparTela();
  }

  private void limparTela() {
    txtId.setText("");
    txtNome.setText("");
    txtRg.setText("");
    txtCpf.setText("");
    txtEstado.setText("");
    txtSexo.setText("");
    txtData.setText("");
    txtLocal.setText("");
  }

  public void onClickBuscar(View v) {

    String id = txtId.getText().toString();

    if (id != null && !id.equals("")) {
      String url = SERVIDOR + "/ws/pessoa/buscar/" + id;
      RequestQueue queue = Volley.newRequestQueue(this);

      JsonObjectRequest request = new JsonObjectRequest(url, null,
          new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject object) {
          try {
            txtId.setText(object.getString("id"));
            txtNome.setText(object.getString("nome"));
            txtRg.setText(object.getString("rg"));
            txtCpf.setText(object.getString("cpf"));
            txtEstado.setText(object.getString("estadoCivil"));
            txtSexo.setText(object.getString("sexo"));

            String dataIn = object.getString("dataNascimento");
            if (dataIn != null && !dataIn.equals("")) {
              try {
                SimpleDateFormat dfin = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date date = dfin.parse(dataIn);
                SimpleDateFormat dfout = new SimpleDateFormat("dd/MM/yyyy");
                txtData.setText(dfout.format(date));
              } catch (ParseException e) {
                e.printStackTrace();
              }
            }

            txtLocal.setText(object.getString("local"));
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
      }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
          Toast.makeText(CadastroActivity.this, "Registro não encontrado!", Toast.LENGTH_LONG).show();
          Log.d("Volley", "Falha na requisição...");
        }
      }) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
          Map<String, String> headers = new HashMap<>();
          headers.put("Accept", "application/json");
          return headers;
        }
      };
      queue.add(request);
      queue.start();
    }
  }

  @NonNull
  private HashMap<String, String> getDados() {

    HashMap<String, String> dados = new HashMap<>();

    String strData = txtData.getText().toString();
    if (strData != null && strData.length() == 10){
      if (strData != null && !strData.equals("")) {
        try {
          SimpleDateFormat dfin = new SimpleDateFormat("dd/MM/yyyy");
          Date date = dfin.parse(strData);
          SimpleDateFormat dfout = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
          dados.put("dataNascimento", dfout.format(date));
        } catch (ParseException e) {
          e.printStackTrace();
        }
      }
    } else if (strData != null && strData.length() > 0) {
      Toast.makeText(CadastroActivity.this, "Use dd/MM/yyyy", Toast.LENGTH_LONG).show();
    }

    String id = txtId.getText().toString();
    if (!id.equals("")) {
      dados.put("id", id);
    }
    dados.put("nome", txtNome.getText().toString());
    dados.put("rg", txtRg.getText().toString());
    dados.put("cpf", txtCpf.getText().toString());
    dados.put("estadoCivil", txtEstado.getText().toString());
    dados.put("sexo", txtSexo.getText().toString());
    dados.put("local", txtLocal.getText().toString());

    return dados;
  }

  public void onClickSalvar(View v) {
    HashMap<String, String> dados = getDados();
    if (dados.get("id") == null && !dados.get("nome").equals("")) {
      String url = SERVIDOR + "/ws/pessoa/salvar";
      salvarOuAtualizar(Request.Method.POST, dados, url);
    } else if(dados.get("id") != null && !dados.get("id").equals("")){
      Toast.makeText(CadastroActivity.this, "Clicar em Atualizar!", Toast.LENGTH_LONG).show();
    } else {
      Toast.makeText(CadastroActivity.this, "Nome é obrigatório!", Toast.LENGTH_LONG).show();
    }
  }

  public void onClickAtualizar(View v) {
    HashMap<String, String> dados = getDados();
    if (dados.get("id") != null && !dados.get("id").equals("")
        && !dados.get("nome").equals("")) {
      String url = SERVIDOR + "/ws/pessoa/atualizar";
      salvarOuAtualizar(Request.Method.PUT, dados, url);
    } else if(dados.get("id") != null && dados.get("id").equals("")){
      Toast.makeText(CadastroActivity.this, "Clicar em Salvar!", Toast.LENGTH_LONG).show();
    } else {
      Toast.makeText(CadastroActivity.this, "Nome é obrigatório!", Toast.LENGTH_LONG).show();
    }
  }

  private void salvarOuAtualizar(final int method, final HashMap<String, String> dados, final String url) {
    RequestQueue queue = Volley.newRequestQueue(this);
    JsonObjectRequest request = new JsonObjectRequest(method,
        url, new JSONObject(dados),
        new Response.Listener<JSONObject>() {

          @Override
          public void onResponse(JSONObject response) {
            Toast.makeText(CadastroActivity.this, "Registro salvo com sucesso!", Toast.LENGTH_LONG).show();
            limparTela();
          }
        }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        Toast.makeText(CadastroActivity.this, "Erro ao salvar registro!", Toast.LENGTH_LONG).show();
        Log.d("Volley", error.getLocalizedMessage());
      }
    }){
      protected Response<JSONObject> parseNetworkResponse(NetworkResponse nr) {
        try {
          String header = HttpHeaderParser.parseCharset(nr.headers, "utf-8");
          String js = new String(nr.data, header);
          JSONObject result = null;
          if (js != null && js.length() > 0) {
            result = new JSONObject(js);
          }
          return Response.success(result, HttpHeaderParser.parseCacheHeaders(nr));
        } catch (Exception e) {
          return Response.error(new ParseError(e));
        }
      }
    };
    queue.add(request);
    queue.start();
  }

  public void onClickExcluir(View v) {

    String id = txtId.getText().toString();

    if (id != null && !id.equals("")) {
      String url = SERVIDOR + "/ws/pessoa/excluir/" + id;
      HashMap<String, String> dados = new HashMap<>();
      dados.put("id", id);

      RequestQueue queue = Volley.newRequestQueue(this);
      JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE,
          url, new JSONObject(dados),
          new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
              Toast.makeText(CadastroActivity.this, "Registro excluído com sucesso!", Toast.LENGTH_LONG).show();
              limparTela();
            }
          }, new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
          Toast.makeText(CadastroActivity.this, "Erro ao excluir registro!", Toast.LENGTH_LONG).show();
          Log.d("Volley", error.getLocalizedMessage());
        }
      }){
        protected Response<JSONObject> parseNetworkResponse(NetworkResponse nr) {
          try {
            String header = HttpHeaderParser.parseCharset(nr.headers, "utf-8");
            String js = new String(nr.data, header);
            JSONObject result = null;
            if (js != null && js.length() > 0) {
              result = new JSONObject(js);
            }
            return Response.success(result, HttpHeaderParser.parseCacheHeaders(nr));
          } catch (Exception e) {
            return Response.error(new ParseError(e));
          }
        }
      };
      queue.add(request);
      queue.start();
    }
  }
}