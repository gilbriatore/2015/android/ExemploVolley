package br.edu.up.exemplovolley;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

  private TextView txtView;
  private ImageView imageView;

  //private final String SERVIDOR = "http://10.0.2.2:9090";
  private final String SERVIDOR = "http://ws-briatore.rhcloud.com";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    txtView = (TextView) findViewById(R.id.textView);
    imageView = (ImageView) findViewById(R.id.imageView);
  }

  public void onClickCadastrar(View v){
    Intent intent = new Intent(this, CadastroActivity.class);
    startActivity(intent);
  }

  //StringRequest GET
  public void onClickCarregarTexto(View v){

    txtView.setText("");
    imageView.setVisibility(View.INVISIBLE);

    String url = SERVIDOR + "/ws/pessoa/listar";
    RequestQueue queue = Volley.newRequestQueue(this);

    StringRequest request = new StringRequest(url,
        new Response.Listener<String>() {
      @Override
      public void onResponse(String response) {
        txtView.setText(response);
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        Log.d("Volley", "Falha na requisição...");
      }
    });

    queue.add(request);
    queue.start();
  }

  //JsonArrayRequest GET
  public void onClickCarregarJson(View v) {

    txtView.setText("");
    imageView.setVisibility(View.INVISIBLE);

    String url = SERVIDOR + "/ws/pessoa/listar";
    RequestQueue queue = Volley.newRequestQueue(this);

    JsonArrayRequest request = new JsonArrayRequest(url,
        new Response.Listener<JSONArray>() {
      @Override
      public void onResponse(JSONArray jsonArray) {

        String jsonTexto = new String();

        for (int i = 0; i < jsonArray.length(); i++) {
          try {
            JSONObject object = (JSONObject) jsonArray.get(i);
            String id = object.getString("id");
            String nome = object.getString("nome");
            String rg = object.getString("rg");
            String cpf = object.getString("cpf");
            String estado = object.getString("estadoCivil");
            String sexo = object.getString("sexo");
            String data = object.getString("dataNascimento");
            String local = object.getString("local");

            jsonTexto += "Pessoa: " + id + " " + nome + " " + rg + " " + cpf + " "
                + estado + " " + sexo + " " + data + " " + local + "\n";

          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
        txtView.setText(jsonTexto);
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        Log.d("Volley", "Falha na requisição...");
      }
    }){
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        return headers;
      }
    };
    queue.add(request);
    queue.start();
  }

  //ImageRequest GET
  public void onClickCarregarImagem(View v) {

    txtView.setText("");
    imageView.setVisibility(View.VISIBLE);

    String url = "https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUVXlkX1NUcEp5N0E";
    RequestQueue queue = Volley.newRequestQueue(this);

    ImageRequest request = new ImageRequest(url,
        new Response.Listener<Bitmap>() {
      @Override
      public void onResponse(Bitmap response) {
        imageView.setImageBitmap(response);
      }
    }, 0, 0, null, null, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        Log.d("Volley", "Falha na requisição...");
      }
    });
    queue.add(request);
    queue.start();
  }
}